# README #

Este Readme corresponde a dar especificaciones de deploy y el funcionamiento de una api rest similar a un catalogo

## Contenido del Repositorio ##

El repositorio contiene:

* Dentro de la carpeta **api-rest** el código fuente de la aplicación junto con los archivos necesarios para ser importados mediante Flyway.
* Dentro de la carpeta **dump** se encuentra una backup de la base de datos
* Dentro de la carpeta **Postman_Workspac**e se encuentra el workspace, en formato json, de Postman utilizado para realizar las pruebas

## Descripción de la api-rest ##

La api fue desarrollada como un proyecto maven utilizando:

* spring-boot 2.4.8.
* java 8

El proyecto base se creo desde [Spring initializr](https://start.spring.io/). El empaquetado elegido como resultado fue **.jar**

Se recomienda utilizar:

* phpMyAdmin.
* Maven 3.8.1.

### Patrones de diseño ###

Se intentó mantener un patrón de diseño por capaz, similar al MVC. En este caso se tienen 4 capaz

1. Controllers: Esta capa se encarga de recibir las peticiones.
2. Servicies: Esta capa contiene la lógica de operación.
3. Repository: Contiene diferente lógica de búsqueda y obtención de datos.
4. Entities: Contiene las clases que se machean directamente con las tablas de las bases de datos.

## Iniciar la aplicación ##

### 1. Generar la base de datos ###

Puede optar por una de las siguientes 3 opciones:

1. Crear su base de datos vacía y modificar el archivo **api-rest\src\main\resources\application.properties**, cambiando el nombre de la base de datos, la url y las credenciales (usuario y contraseña). Actualmente la url apunta a la dirección por defecto phpMyAdmin y el nombre de la base de datos es **catalog_db**.

2. Utilizar phpMyAdmin y crear la base de datos con el nombre **catalog_db**.

3. Utilizando phpMyAdmin importar la base de datos que se encuentra en la carpeta **dump**. Recordar que este es un back, con lo cual ya contiene datos.

### 2. Importar los datos ###

La api levanta en la url **http://localhost:8080/**. Si tiene alguna aplicación, servicio o el puerto está bloqueado no levantara

La importación de datos se hace de forma automática, mediante dos alternativas:

1. Utilizando **Flyway** desde dos archivos ubicados en **\api-rest\src\main\resources\db\migration\\**

2. Comentando desde el archivo application.properties las referencias de Flyway y descomentando el bean del archivo **api-rest\src\main\java\com\glamit\challenge\apirest\ApiRestApplication.java**. Junto con otras líneas comentadas, indicadas en el código.

### 3. Levantar la api ###

Para levantar la api, simplemente es necesario ejecutar ubicarse dentro de la carpeta api-rest, y ejecutar el siguiente comando (Recordar tener acceso a la base de datos)

```
    mvnw spring-boot:run
```

## Uso de la api ##

Una vez levantada la aplicación, esta cuenta con tres end points:

1. **http://localhost:8080/** el cual atiende peticiones **GET** y devuelve todas las categorías del catalogo. Header necesario: **categories_header**

2. **http://localhost:8080/product_list/** el cual atiende peticiones **GET** y devuelve todos los productos del catálogo, posee opciones de paginado y filtrado por SKU. Para acceder a estas opciones, es necesario agregar a la url:
    * ?size para especificar el tamaño de la página.
    * ?page para especificar a qué página acceder.
    * ?sku para buscar un sku particular, esta búsqueda no funciona si no se coloca exactamente el sku, ya que no busca si contiene o no la cadena de texto.
    Header necesario **products_header**

3. **http://localhost:8080/product** el cual atiende peticiones del tipo **POST**. Es utilizado para agregar un producto a la base de datos, para esto es necesario pasarle un json como el siguiente:

    ```json
    {
        "sku": "abc-121",
        "name": "Zapatillas Deportivas",
        "price": 100.6,
        "urlImage": "http://apatillas_deportivas",
        "categoryId": 4
    }
    ```

    Si el SKU se encuentra en la base de datos el producto no podrá ser creado, de igual manera si se pasa un id de categoría que no se encuentre en la base de datos. Header necesario **add_product_header**
    **__Aclaración:__** Los mensajes de error se encuentran en español suponiendo un cliente en español. Los comentarios dentro de la api están todos en inglés.

Para probar la api se puede utilizar postman e importar el workspace de prueba desde la carpeta **Postman_Workspace**.

## Repositorio ##

El repositorio se encuentra en BitBucket y puede ser accedido desde [aquí](https://bitbucket.org/warinet/api-rest_challenge)
