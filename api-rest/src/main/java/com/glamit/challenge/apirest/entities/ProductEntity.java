package com.glamit.challenge.apirest.entities;

import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.persistence.*;

@Entity
//@SequenceGenerator(name="PRODUCT_SEQ",  initialValue=1) // Uncomment to not use sql files
@Table(name = "product")
public @Data
class ProductEntity {

    //==================================
    //========= Attributes =============
    //==================================
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="PRODUCT_SEQ") // Uncomment to not use sql files
    private long ID;

    @NotBlank
    @Column(unique = true,nullable = false)
    private String SKU;

    @Column(name="PRODUCT_NAME")
    private String name;
    @Column(name="PRODUCT_PRICE")
    private float price;
    @Column(name="PRODUCT_IMAGE")
    private String urlImage;

    private long categoryId;

    @ManyToOne
    @JoinColumn(name="CATEGORY")
    private CategoryEntity category;

    //==================================
    //========= Constructors ===========
    //==================================

    private ProductEntity(){}

    public ProductEntity(String SKU,String name, CategoryEntity category, float price, String urlImage){
        this();
        this.SKU = SKU;
        this.name = name;
        this.category = category;
        this.price = price;
        this.urlImage = urlImage;
    }

    //==================================
    //========= Getters ================
    //==================================

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getSKU() {
        return SKU;
    }

    public long getID() {
        return ID;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public long getCategoryId() {
        return categoryId;
    }

    //==================================
    //========= Setters ================
    //==================================


    public void setSku(String skuProduct) {
        this.SKU = skuProduct;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "SKU: '" + SKU + '\'' +
                ", Name: '" + name + '\'' +
                ", Price: " + price +
                ", Image: '" + urlImage + '\'' +
                ", Category: " + category +
                '}';
    }
}
