package com.glamit.challenge.apirest.controllers;

import com.glamit.challenge.apirest.entities.CategoryEntity;
import com.glamit.challenge.apirest.entities.ProductEntity;
import com.glamit.challenge.apirest.servicies.CategoryEntityService;
import com.glamit.challenge.apirest.servicies.ProductEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogEntityController {

    @Autowired
    private CategoryEntityService categoryEntityService;

    @Autowired
    private ProductEntityService productEntityService;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryEntity> listCategories(
            @RequestHeader(name = "categories_header", required = true) String categoriesHeader) {
        return categoryEntityService.getCategories();
    }

    @GetMapping("/product_list")
    @ResponseStatus(HttpStatus.OK)
    public Page<ProductEntity> listProducts(
            @RequestHeader(name = "products_header", required = true) String productsHeader,
            @PageableDefault(size = 10, page = 0) Pageable pageable, @RequestParam(required = false) String sku) {

        if (sku!=null){
            return productEntityService.findAllBySKU(pageable,sku);
        }
        return productEntityService.findAll(pageable);
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> addProduct(
            @RequestHeader(name = "add_product_header", required = true) String addProductHeader,
            @RequestBody final ProductEntity product) {

        if (categoryEntityService.existCategoryById(product.getCategoryId())) {
            product.setCategory(categoryEntityService.getCategoryById(product.getCategoryId()));
            return productEntityService.addProduct(product);
        }else{
            return new ResponseEntity("El id de la categoría no existe en la base de datos",HttpStatus.NOT_FOUND);
        }

    }

}
