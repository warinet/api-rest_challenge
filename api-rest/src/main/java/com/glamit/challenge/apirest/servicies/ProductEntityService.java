package com.glamit.challenge.apirest.servicies;

import com.glamit.challenge.apirest.entities.ProductEntity;
import com.glamit.challenge.apirest.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductEntityService {

    @Autowired
    private ProductRepository productRepository;

    public Page<ProductEntity> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    public Page<ProductEntity> findAllBySKU(Pageable pageable, String SKU) {
        return productRepository.findBySKU(SKU,pageable);
    }

    public ResponseEntity addProduct(ProductEntity product) {
        if (productRepository.findBySKU(product.getSKU()).isEmpty()){
            productRepository.save(product);
            return new ResponseEntity("El producto fue creado exitosamente", HttpStatus.CREATED);
        }
        return new ResponseEntity("Ya existe el SKU en la base de datos",HttpStatus.IM_USED);
    }

    public List<ProductEntity> findProductBySKU(String SKU) {
        return productRepository.findBySKU(SKU);
    }

}
