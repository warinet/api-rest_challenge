package com.glamit.challenge.apirest.repositories;

import com.glamit.challenge.apirest.entities.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends PagingAndSortingRepository<ProductEntity, Long> {
    Page<ProductEntity> findBySKU(String SKU, Pageable pageable);
    List<ProductEntity> findBySKU(String SKU);
}
