package com.glamit.challenge.apirest.repositories;

import com.glamit.challenge.apirest.entities.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<CategoryEntity, Long> {

}
