package com.glamit.challenge.apirest.servicies;

import com.glamit.challenge.apirest.entities.CategoryEntity;
import com.glamit.challenge.apirest.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryEntityService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<CategoryEntity> getCategories() {
        List<CategoryEntity> categories = new ArrayList<>();
        categoryRepository.findAll().forEach(categories::add);
        return categories;
    }

    public boolean existCategoryById(long id) {
        return  categoryRepository.existsById(id);
    }
    public CategoryEntity getCategoryById(long id) {
        return  categoryRepository.findById(id).get();
    }

}
