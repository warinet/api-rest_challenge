package com.glamit.challenge.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestApplication.class, args);
	}

	// If yo don't want use any kind of .sql yo can uncomment this Bean add categories manually
	// Remember uncomment lines in the application.properties, it CategoryEntity, and ProductEntity
	/*@Bean
	public CommandLineRunner loadData(CategoryRepository categoryRepository) {
		return (args) -> {
			categoryRepository.save(new CategoryEntity("Pantalon"));
			categoryRepository.save(new CategoryEntity("Remera"));
			categoryRepository.save(new CategoryEntity("Zapatos"));
			categoryRepository.save(new CategoryEntity("Zapatillas"));
		};
	}*/

}
