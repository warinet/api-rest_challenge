package com.glamit.challenge.apirest.entities;

import javax.persistence.*;

@Entity
//@SequenceGenerator(name="CATEGORY_SEQ",  initialValue=1) // Uncomment to not use sql files
@Table(name = "category")
public class CategoryEntity {

    //==================================
    //========= Attributes =============
    //==================================

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="CATEGORY_SEQ") // Uncomment to not use sql files
    @Column(name = "CATEGORY_ID")
    private long ID;
    @Column(name = "CATEGORY_NAME")
    private String name;

    //==================================
    //========= Constructors ===========
    //==================================
    private CategoryEntity(){}

    public CategoryEntity(String name){
        this.name = name;
    }

    //==================================
    //========= Getters ================
    //==================================

    public String getName() {
        return name;
    }

    public long getID() {
        return ID;
    }

    //==================================
    //========= Setters ================
    //==================================

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "ID:" + ID+
                ", Name:'" + name + '\'' +
                '}';
    }
}
