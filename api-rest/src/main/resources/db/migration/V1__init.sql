--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY UK_category_name (`category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `category` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY UK_sku (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indices de la tabla `product`
--
ALTER TABLE product ADD KEY `FKqx9wikktsev17ctu0kcpkrafc` (`category`);

--
-- Filtros para la tabla `product`
--
ALTER TABLE product ADD CONSTRAINT `FKqx9wikktsev17ctu0kcpkrafc` FOREIGN KEY (`category`) REFERENCES `category` (`category_id`); 
