--
-- Volcado de datos para la tabla `category`
--

INSERT INTO category(category_name) VALUES ('Pantalon'), ('Remera'), ('Zapatos'), ('Zapatillas');

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO product(sku, category_id, product_name, product_price, product_image, category) VALUES ('abc-126', 1, 'Pantalon Largo', 100.6, 'http://pantalon_largo', 1);
INSERT INTO product(sku, category_id, product_name, product_price, product_image, category) VALUES ('abc-127', 2, 'Remera Mangas Corta', 51.6, 'http://remera_mangas_corta', 2);
INSERT INTO product(sku, category_id, product_name, product_price, product_image, category) VALUES ('abc-128', 3, 'Zapatos Marron', 200.5, 'http://Zapatos_marron', 3);

